/**
 * We Dream in Color.
 * draw some colored shapes with De Jong attractor.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.10.13
 */

void setup() {

  size(720, 720);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  blendMode(DIFFERENCE);
  smooth();
  noStroke();

}

void draw() {

  translate(width / 2, height / 2);

  int   shapeMax = 5;
  int   frmMax   = 5;
  float baseHue  = random(360);

  for(int frmCnt = 0; frmCnt < frmMax; ++frmCnt) {

    float frmRatio  = map(frmCnt, 0, frmMax, 0.0, 1.0);
    float shapeSeed = random(1.0);
    float vSat = 40.0 + frmRatio * 40.0;
    float bSat = 60.0 - frmRatio * 30.0;
    baseHue += 60.0;
  
    background(baseHue % 360.0, bSat, 80.0, 100.0);
    for(int shapeCnt = 0; shapeCnt < shapeMax; ++shapeCnt) {

      float shapeRatio = map(shapeCnt, 0, shapeMax, 0.0, 1.0);

      // magic spell
      float seed = map(sin(TWO_PI * (shapeSeed + shapeRatio)), -1.0, 1.0, 0.6, 1.3);
      float pA   = seed + random(0.1);
      float pB   = seed + random(0.1);
      float pC   = -seed + random(0.1);
      float pD   = -seed + random(0.1);

      int   lineMax = 10 + floor(shapeRatio * 100);
      float prevX   = random(-1.0, 1.0);
      float prevY   = random(-1.0, 1.0);
      float vHue    = baseHue + (shapeCnt % 3) * 45.0;
      float vBri    = map(shapeCnt % 5, 0, 4, 60.0, 90.0);

      pushMatrix();
      translate(width * random(-0.1, 0.1), height * random(-0.1, 0.1));
      rotate(TWO_PI * shapeRatio);
      curveTightness(random(-5.0, 0.0));
      beginShape();
      for (int lineCnt = 0; lineCnt < lineMax; ++lineCnt) {
        float lineRatio = map(lineCnt, 0, lineMax, 0.0, 1.0);
        // De Jong attractor + alpha
        float currX = sin(pA * prevY) * cos(pB * prevX) + prevX * lineRatio * 0.05;
        float currY = sin(pC * prevX) * cos(pD * prevY) + prevY * lineRatio * 0.05;
        prevX = currX;
        prevY = currY;

        fill(vHue % 360.0, vSat, vBri, 100.0);
        curveVertex(currX * width * 0.5, currY * height * 0.5);
      }
      endShape();
      popMatrix();
    }
    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");
  }
  exit();
}
